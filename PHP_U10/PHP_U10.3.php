﻿<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
<link rel="stylesheet" href="../StickyHeader.CSS">
<link rel="stylesheet" href="../Aufgabe.css">
<link rel="stylesheet" href="Grid.CSS">
<title>PHP_Ü10</title>
</head>
<body>

<?PHP

if(isset($_POST["hidden"])){

    $SALT = "iererdf /%%)9fff2FbjbbkEdypqeo4) 33weflv===99fdf8  byx iro421    d dsddsd3440fafv odsf//1933";

    $username = hash("sha384",$_POST["usrN"].SALT);
    $password = hash("sha384",$_POST["pw"].SALT);

    $file = "./logindata.csv";
    $new_line = $username.",".$password."\n";

    // Check if User is already registered

    $i = 0;
    $notregistered = true;

    // Source Code for reading CSV in PHP Files:
    // https://www.edv-lehrgang.de/php-funktion-fgetcsv/

    if(($handle = fopen("logindata.csv","r")) !== FALSE) {
		while(($login = fgetcsv($handle,1000,",")) !== FALSE) {
			if($username === $login[$i] && $password === $login[++$i]){
				$status = "Sie sind bereits registiert";
				$notregistered = false;
				break;
			}

		}
		fclose($handle);
    }

    if($notregistered){
	if(file_put_contents($file,$new_line,FILE_APPEND|LOCK_EX)){
    	    $status = "Sie wurden registriert";
        }
    }
}

if(isset($_POST["action"])){

    $loginusrname = hash("sha384",$_POST["usrName"].SALT);
    $loginpassword = hash("sha384",$_POST["password"].SALT);

    $i = 0;
    $status = "Login fehlgeschlagen!";

	// Source Code for reading CSV in PHP Files:
	// https://www.edv-lehrgang.de/php-funktion-fgetcsv/

	if(($handle = fopen("logindata.csv","r")) !== FALSE) {
		while(($login = fgetcsv($handle,1000,",")) !== FALSE) {
			if($loginusrname === $login[$i] && $loginpassword === $login[++$i]){
				$status = "Login erfolgreich!";
				break;
			}

		}
		fclose($handle);
	}
}


?>

<!-- Script Section -->

<script src="../Button.js"></script> 
<script src="../JSONScript.js"></script>
<script src="../PageOnLoad.js"></script>

<!-- Header Section -->

<a id="linkHomepage" href="../index.html"></a>

<div id="header" class = "Header">
	<img id="home" class="button" src="../HomeIcon.png" alt="Home button" onclick="goHome()">
	<img id="back" class="button" src="../Back.png" alt="Back button" onclick="goForward_Back(true)">
	<a class="link" href="PHP_U10.html">Ü10.1 Registrierung mit PHP</a>
    <a class="link" href="PHP_U10.html">Ü10.2 Login mit PHP</a>
    <a class="link" href="javascript:loadJson('https://www2.inf.h-bonn-rhein-sieg.de/~mschm82s/PHP_U10/PHP_U10.3.json',true);">Ü10.3 WWW-Navigator zum Content-Editor ausbauen</a>
	<img id="forward" class="button" src="../Forward.png" alt="Forward button" onclick="goForward_Back(false)">
</div>

<!-- Content Section -->

<div id="ParaAufgabe" class="Aufgabe"></div>

<form action="PHP_U10.3.php" method="post">
  <fieldset>
  <legend>Register:</legend>
  <label for="usrN">Username:</label><br>
  <input type="text" id="usrN" name="usrN" required><br>
  <label for="pw">Password:</label><br>
  <input type="password" id="pw" name="pw" required><br><br>
  <input type="submit" value="Confirm" onclick=notify()>
  <input type="hidden" name="hidden" value=1>
  </fieldset>
</form>

<form action="PHP_U10.3.php" method="post">
  <fieldset>
  <legend>Login:</legend>
  <label for="usrName">Username:</label><br>
  <input type="text" id="usrName" name="usrName" required><br>
  <label for="password">Password:</label><br>
  <input type="password" id="password" name="password" required><br><br>
  <input type="submit" value="Enter" onclick=notify()>
  <input type="hidden" name="action" value=1>
  </fieldset>
</form>


<script>

function notify(){

    const notification = "<?PHP echo $status ?>";

    window.alert(notification);
}

</script>

</body>
</html>