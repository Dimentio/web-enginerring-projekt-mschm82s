﻿function goForward_Back(direction) {
	
	/*	
		A button which traverses the given list of links in the header forward and backward.
		Each step calls the underlying Eventlisteners to change the displayed content accordingly.
		
		Parameter:
			-> diretion: true = traverses backwards / false = traverses forward
	*/
	
	const header = document.getElementById("header");
	const items = header.getElementsByTagName("a");
	
	for(node of items) {
		if( direction === true) {
			if(node.classList.contains("marked") && node != items[0]) {
				node.previousElementSibling.dispatchEvent(new Event("click"));
				node.previousElementSibling.click();
				return;
			}
		} else {
			if(node.classList.contains("marked") && node != items[items.length-1]) {
				node.nextElementSibling.dispatchEvent(new Event("click"));
				node.nextElementSibling.click();
				return;
			}	
		}
	}	
	return;
}
function goHome() {

	/* 
		A button which returns the User to the Homepage
	*/

    // Load index.html

    document.getElementById('linkHomepage').click();

    // Remove the local Session ID when returning home

    sessionStorage.removeItem("markedfield");
}