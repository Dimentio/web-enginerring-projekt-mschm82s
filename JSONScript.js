﻿async function fetchJson(file) {
	
	/* 
		Calls a fetch request with given URL Path and parses the response as JSON.
		Returns the result as JSON Object.
		Requires:
			-> Valid Path URL to JSON file	
	*/
	
           let response;
           let jsObject;
            try{
                response = await fetch(file);
                jsObject = await response.json();
            } catch(error) {
                console.log("Could not fetch the Json File. Please check if the given URL Path is correct or if the server is currently inactive. Error log: " + error);
                window.alert("Could not fetch the Json file. \
				Please disable any Script blocks for this application to run propperly. \
				Otherwise the server may be currently unavailable");
            }
            return jsObject;
}
function display(json, preformat = false) {
	
	/* 
		Accepts a JSON object in Array of Objects notation and and appends each value
		as paragraph (<p>) or preformated in JSON <pre> to a parent element with the ID: 'ParaAufgabe'.
		
		Requires: 
			-> JSON Object in Array of Objects notation,
			-> A parent Element in the source file with the ID: 'ParaAufgabe',
			-> For the right CSS display the keys of the Objects within JSON have to be either called:
				'subH' for Heading as textcontent, 
				'subQ' for Question/Task as textcontent,
				'subA' for Answer as textcontent
		Optional:
			-> 2nd Parameter 'preformat' will display Parapgraphs as <p> by default and as <pre> if set true
	*/
	
	
	json.then(json => { 
		
		const paradiv = document.getElementById("ParaAufgabe");
		const jsonobjects = Object.values(json);
		
		if(paradiv.hasChildNodes) {
		
			// Source Code for remove all children function:
			// https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
			
			while(paradiv.firstChild) {
				paradiv.removeChild(paradiv.lastChild);
			}
		}

		for(let text of jsonobjects) {
			
			let length = Object.values(text).length;
			let container = [];
			
			for(let i=0;i<length;i++) {
			
				if(preformat) {
					container.push(document.createElement("pre"));
				} else {	
					container.push(document.createElement("p"));
				}
			}

			const values = Object.values(text);
			const keys = Object.keys(text);

			for(let x=0; x<container.length;x++){
				
				if(keys[x] === "subH"){
				
					container[x].classList.add("textAufgabeHeader");
			
				} else if(keys[x] === "subQ") {
				
					container[x].classList.add("textAufgabe");
					
				} else if(keys[x] === "subA") {
					
					container[x].classList.add("textAntwort");
				
				}
				
				container[x].textContent = values[x];
				paradiv.appendChild(container[x]);
			}
		}
	});
	return;
}
function displayParagraphs(json, preformat=false){
	
	/* 
		Another display version which appends the values of the JSON Object to more than 1 parent element. 
		
		NOTE: Currently only supports 2 Paragraphs 'ParaAufgabe' and 'ParaAufgabe2'. 
		Can be adjusted to a more dynmaic solution in the future
	*/
		
	json.then(json => {
		
		const paradiv = document.getElementById("ParaAufgabe");
		const paradiv2 = document.getElementById("ParaAufgabe2");
		
		if(document.body.contains(paradiv2)) {
			
			
			if(paradiv.hasChildNodes) {
		
			// Source Code for remove all children function:
			// https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
			
				while(paradiv.firstChild) {
					paradiv.removeChild(paradiv.lastChild);
				}
			}
			
			if(paradiv2.hasChildNodes) {
		
			// Source Code for remove all children function:
			// https://stackoverflow.com/questions/3955229/remove-all-child-elements-of-a-dom-node-in-javascript
			
				while(paradiv2.firstChild) {
					paradiv2.removeChild(paradiv2.lastChild);
				}
			}
			
			let aufgabe1;
			let aufgabe2;
			
			if(preformat) {
				aufgabe1 = document.createElement("pre");
				aufgabe2 = document.createElement("pre");
			} else {
				aufgabe1 = document.createElement("p");
				aufgabe2 = document.createElement("p");
			}
			aufgabe1.classList.add("textAufgabe");
			aufgabe2.classList.add("textAufgabe");

			aufgabe1.textContent = Object.values(Object.values(Object.values(json))[0]);
			aufgabe2.textContent = Object.values(Object.values(Object.values(json))[1]);

			paradiv.appendChild(aufgabe1);
			paradiv2.appendChild(aufgabe2);
			
			return;
		}
		
	});

}
function loadJson(path,preformat=false){
	
	/* 
		A function which uses the given URL path and preformat Parameter
		to fetch a JSON file and displays it with the default display function
	*/
	
	display(fetchJson(path),preformat);
	
	return;
}
function loadJsonParagraphs(path,preformat=false){
	
	/* 
		A function which uses the given URL path and preformat Parameter
		to fetch a JSON file and displays it with the paragraphs display version
	*/
	
	displayParagraphs(fetchJson(path),preformat);
	
	return;
}