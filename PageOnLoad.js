﻿window.onload = function() {
	
	// Create EventListner which listen to mouse-moves over the header Icons and change the images on the Buttons accordingly.
	
	var forward = document.getElementById("forward");
	forward.addEventListener("mouseenter", function(){
		forward.src = "../Forward_Blue.png";
	},false);
	forward.addEventListener("mouseleave", function(){
		forward.src = "../Forward.png";
	},false);
	
	var backward = document.getElementById("back");
	backward.addEventListener("mouseenter", function(){
		backward.src = "../Back_Blue.png";
	},false);
	backward.addEventListener("mouseleave", function(){
		backward.src = "../Back.png";
	},false);
	
	var home = document.getElementById("home");
	home.addEventListener("mouseenter", function(){
		home.src = "../HomeIcon_Blue.png";
	},false);
	home.addEventListener("mouseleave", function(){
		home.src = "../HomeIcon.png";
	},false);

	// Create EventListener for each link in the header, which mark the current selected link
	
	const header = document.getElementById("header");
	const items = header.getElementsByTagName("a");
		
	for(let i=0;i<items.length;i++){
			items[i].addEventListener("click", function(){	
				for(let a=0;a<items.length;a++){
					items[a].classList.remove("marked");
				}
				this.classList.add("marked");
				sessionStorage.setItem("markedfield", i);
			},false);
	}
	
	/* 
	// On new subpage get the saved index to call the right marker EventListener. And trigger the according link.
	// Check strictly if the session item is either 'undefined' or 'null' or 'OutOfBounce' due to previous session storages.
	// If so call the first marker EventListener with the link by default.
	*/
	
	const id = sessionStorage.getItem("markedfield");
	
	if((id === null) || (id === undefined) || (id >= items.length)){
		items[0].dispatchEvent(new Event("click"));
		items[0].click();
	} else {
		items[id].dispatchEvent(new Event("click"));
		items[id].click();
	}
}